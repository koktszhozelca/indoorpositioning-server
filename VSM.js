const { Zone, Measure } = require('./Models')

class VSM {
  static parse({ rssis, zoneKey, size }) {
    return rssis;
  }

  static query(measurement) {
    console.log("Received an query", measurement)
    try {
      let allMeasurements = Measure.query({ fields: "{zoneKey, rssis}", get: false })      
      if (!measurement.length || !allMeasurements || !allMeasurements.length) {
        let res = { zone: Zone.get("4e715f4d8d2919c6d08c3295d70514978DEbA"), similarity: 0 }
        return res
      }
      let similarities = {}
      allMeasurements.map(record => {
        if (!similarities[record.zoneKey])
          similarities[record.zoneKey] = []
        similarities[record.zoneKey].push(...record.rssis)
      })
      return VSM.getSimilarityForZones(similarities, measurement)
    } catch (err) {
      console.log(err)
      return { status: 500, message: "Can't get the location right now." }
    }
  }

  /*
   zoneMeasurement 
   {
      zoneKey: [ [rssis...], [rssis...], [rssis...], [rssis...] ],
      zoneKey: [ [rssis...], [rssis...], [rssis...], [rssis...] ],
      zoneKey: [ [rssis...], [rssis...], [rssis...], [rssis...] ]
   }

   measurement
  [ [rssis...], [rssis...], [rssis...], [rssis...] ]

   Maintain the similarity array with the same length of measure,
   which stores the closest similarity zone information.
      [ 
          { zoneKey, similarity }, { zoneKey, similarity }, 
          { zoneKey, similarity }, { zoneKey, similarity } 
      ]
   
   Sort by the following rules
   1. Zone count
   2. Similarity
    
   Return the first entry.
  */
  static getSimilarityForZones(zoneMeasurements, measurement) {
    let results = []
    measurement = Measure.normalize(measurement)
    Object.keys(zoneMeasurements).map(zoneKey => {
      let zoneMeasurement = zoneMeasurements[zoneKey]
      measurement.forEach((rssis, index) => {
        zoneMeasurement.forEach(zoneRssis => {
          let similarity = VSM._cosSim(zoneRssis, rssis)
          if (!results[index]) results[index] = { zoneKey, similarity }
          else
            if (similarity > results[index].similarity)
              results[index] = { zoneKey, similarity }
        })
      })
    })
    let zoneKeys = results.map(res => res.zoneKey)
    let topZone = [...new Set(zoneKeys)].map(zoneKey => {
      let count = 0;
      zoneKeys.map(key => count += key === zoneKey ? 1 : 0)
      return { zoneKey, count };
    }).sort((a, b) => b.count - a.count)[0].zoneKey
    let sim = results.filter(res => res.zoneKey === topZone)
      .sort((a, b) => b.similarity - a.similarity)[0].similarity
    console.log("Response query", { zone: Zone.get(topZone), similarity: sim })
    return { zone: Zone.get(topZone), similarity: sim }
  }

  static _cosSim(vecA, vecB) {
    let sim = 0;
    for (let i = 0; i < vecA.length; i++) sim += vecA[i] * vecB[i]
    return sim
  }
}
module.exports = VSM