console.clear()
const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')

const Zetabase = require('zetabasejs')
const db = new Zetabase('./database.json', { orm: true })

const { Zone, Measure, iBeacon } = require('./Models')
const VSM = require('./VSM')
const Debug = require('./Debug')

const config = {
    PORT: 9300
}
const app = express()
app.use(cors()).use(bodyParser.json())
app.get('/', express.static('public'))
    // iBeacon
    .get('/ibeacon', (r, s) => s.json(iBeacon.read(r.query.identifier)))
    .post('/ibeacon/batch', (r, s) => s.json(iBeacon.batchCreate(r.body.data)))
    .post('/ibeacon', (r, s) => s.json(iBeacon.create(r.body.identifier, r.body.x, r.body.y)))
    .put('/ibeacon', (r, s) => s.json(iBeacon.update(r.body.identifier, r.body.x, r.body.y)))
    // Zone
    .post('/zone', (r, s) => s.json(Zone.create(r.body.label, r.body.description)))
    .post('/zone/batch', (r, s) => s.json(Zone.batchCreate(r.body.data)))
    .get('/zone', (r, s) => s.json(Zone.read(r.query.label)))
    .get('/zone/detail', (r, s) => s.json(Zone.detail(r.query.label)))
    .put('/zone', (r, s) => s.json(Zone.update(r.body.key, r.body.label, r.body.description)))
    // Measure
    .post('/measure', (r, s) => s.json(Measure.create(r.body.zoneKey, r.body.rssis)))
    .post('/measure/batch', (r, s) => s.json(Measure.batchCreate(r.body.zoneKey, r.body.rssis)))
    .get('/measure', (r, s) => s.json(Measure.read(r.query.zoneKey)))
    // Query
    .post('/query', (r, s) => s.json(VSM.query(r.body.rssis)))
    // Heartbeat
    .get('/heartbeat', (r, s) => s.json({ status: "okadfasdfsdf" }))
    // Extra
    .post('/wipe', (r, s) => {
        console.log(r.body.class + " is wipe.")
        if (db.containsKey(r.body.class))
            db.wipe(`/${r.body.class}`)
        s.send('DONE')
    })
    // Debug
    .post('/reset', (r, s) => { db.wipe('/'); db.invalidate(); s.send("Database is reset."); })
    .get('/db', (r, s) => s.json(db.memory))
    .get('/test', (r, s) => s.json(Debug.test()))
    .listen(config.PORT, _ => console.log("Server listens on port " + config.PORT))