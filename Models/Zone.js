const { Model } = require('zetabasejs/orm')
const Measure = require('./Measure')
class Zone extends Model {
    constructor(label, description = null) {
        super()
        this.label = label
        this.description = description
    }

    static batchCreate(data) {
        return data.map(param => {
            let zone = null
            if (param.__key__) {
                try { zone = Zone.get(param.__key__) }
                catch (err) { console.log(err); zone = new Zone() }
                zone.label = param.label;
                zone.description = param.description;
                return zone.put()
            } return new Zone(param.label, param.description).put()
        })
    }

    static create(label, description = null) {
        return new Zone(label, description).put()
    }

    static read(label = null) {
        let results = label ? Zone.query({
            filters: { label: v => v === label }
        }) : Zone.query()
        return { num_results: results ? results.length : 0, results }
    }

    static update(key, label, description = null) {
        let zone = Zone.get(key)
        zone.label = label
        zone.description = description
        return zone.put()
    }

    static detail(label = null) {
        let zoneData = Zone.read(label)
        let measurementData = Measure.read();
        if (!measurementData.num_results) return { num_results: 0, results: [] }
        let results = zoneData.results.map(zone => {
            let relatedMeasurements = (measurementData.results || []).filter(measurement => measurement.zoneKey === zone.__key__)
            return { zone, relatedMeasurements }
        })
        return { num_results: results.length, results }
    }
}
module.exports = Zone