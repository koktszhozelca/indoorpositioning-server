const { Model } = require('zetabasejs/orm')
class iBeacon extends Model {
    constructor(identifier, x, y) {
        super()
        this.identifier = identifier;
        this.x = x;
        this.y = y;
    }

    static batchCreate(data) {
        return data.map(param => {
            let ibeacon = null
            if (param.__key__) {
                try { ibeacon = iBeacon.get(param.__key__) }
                catch (err) { console.log(err); ibeacon = new iBeacon() }
                ibeacon.identifier = param.identifier;
                ibeacon.x = param.x;
                ibeacon.y = param.y;
                return ibeacon.put()
            } return new iBeacon(param.identifier, param.x, param.y).put()
        })
    }

    static create(identifier, x, y) {
        return new iBeacon(identifier, x, y).put()
    }

    static read(identifier = null) {
        let results = identifier ? iBeacon.query({
            filters: { identifier: v => v === identifier }
        }) : iBeacon.query()
        return { num_results: results ? results.length : 0, results }
    }

    static update(key, identifier, x, y) {
        let ibeacon = iBeacon.get(key)
        ibeacon.identifier = identifier;
        ibeacon.x = x;
        ibeacon.y = y;
        return ibeacon.put()
    }
}
module.exports = iBeacon