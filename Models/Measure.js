const { Model } = require('zetabasejs/orm')
const iBeacon = require("./iBeacon")
const { MAX_RECORD_PER_ZONE } = require("../Config")

class Measure extends Model {
    constructor(zoneKey, rssis) {
        super()
        this.zoneKey = zoneKey;
        this.rssis = rssis && rssis.length > 0 ? Measure.normalize(rssis) : rssis
    }

    static normalize(measurement) {
        let normalized = []
        measurement.forEach(rssis => normalized.push(Measure._normalize(rssis)))
        return normalized
    }

    static _normalize(rssis) {
        let sum = 0, magnitude = 0, normalized = []
        rssis.forEach(v => sum += Math.pow(v, 2))
        magnitude = Math.sqrt(sum)
        rssis.forEach(v => normalized.push(v / magnitude))
        return normalized;
    }

    static create(zoneKey, rssis) {
        return new Measure(zoneKey, rssis).put()
    }

    static read(zoneKey) {
        let results = zoneKey ? Measure.query({
            filters: { zoneKey: v => v === zoneKey }
        }) : Measure.query()
        return { num_results: results ? results.length : 0, results }
    }

    static batchCreate(zoneKey, rssis) {
        if (Measure.read(zoneKey).num_results >= MAX_RECORD_PER_ZONE)
            return console.log("ERROR: Maximum record per zone is reached, zoneKey: " + zoneKey)
        let measure = new Measure(zoneKey, rssis).put()
        console.log("INFO: New measurement is reveiced", measure);
        return measure
    }
}

module.exports = Measure;